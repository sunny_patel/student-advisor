import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Facebook } from 'ionic-native';
import { Slides } from 'ionic-angular';

import * as firebase from 'firebase';
import * as _ from 'underscore';

@Component({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {
  @ViewChild('mainSlider') slider: Slides;

  user: any;
  mainSliderOptions: any;

  constructor(public navCtrl: NavController) {
    this.user = {};
    this.mainSliderOptions = {
      onlyExternal: true
    }
  }

  facebookLogin() {
    Facebook.login(['email', 'public_profile'])
    .then((results) => {
      let facebookUser = results.authResponse;
      let provider = new firebase.auth.FacebookAuthProvider();
      console.warn('******* FB ******');
      console.warn(JSON.stringify(facebookUser));
      let credential = (<any> firebase.auth.FacebookAuthProvider).credential(facebookUser.accessToken);
      let firebaseUser = firebase.auth().currentUser;
      console.warn("here here herer");
      console.warn("firebaseUser : ");
      console.warn(JSON.stringify(firebaseUser));
      let needReAuthFireBase = this.isUserEqual(facebookUser, firebaseUser);
      console.warn("isUserEqual = " + needReAuthFireBase);
      console.warn(JSON.stringify(needReAuthFireBase));
      if (!needReAuthFireBase) {
        // Firebase Auth has expired, we need to relogin
        firebase.auth().signInWithCredential(credential)
        .then((results) => {
          // Firebase login successful!
          console.warn("************ FIREBASE AUTH ************");
          let userRef = firebase.database().ref('users/' + results.uid);
          console.warn(JSON.stringify(userRef));
          let userUniRef = firebase.database().ref('users/' + results.uid + '/university');
          console.warn(JSON.stringify(userUniRef))
          console.warn("************ FIREBASE AUTH END ************");
          // // Find user with uid in '/users'
          // usersRef.equalTo(results.uid)
        })
        .catch((error) => {
          console.error("home.ts: facebookLogin: firebase.auth catch");
          console.error(JSON.stringify(error));
        })
      } else {
        // Already signed in with Firebase and this Provider account, no reauth
        console.warn("************ FIREBASE ************");
        console.warn("CURRENT USER : " + JSON.stringify(firebaseUser));
        let userRef = firebase.database().ref('users/' + firebaseUser.uid);
        userRef.once("value")
        .then((snapshot) => {
          // Check if user profile is filled out
          if (snapshot.exists() && snapshot.child('firstName').exists()) {
            // Redirect to App page, profile already filled out
            _.extend(this.user, snapshot.val());
          } else {
            console.warn('YO YO YO Y');
            console.warn(firebaseUser.displayName);
            console.warn(firebaseUser.displayName + "  " + firebaseUser.displayName.indexOf(' '));
            // Fill out profile info, programmatically swipe profile form, so user can fill out rest of profile
            this.user.firstName = firebaseUser.displayName.substring(0, firebaseUser.displayName.indexOf(' '));
            this.user.lastName = firebaseUser.displayName.substring(firebaseUser.displayName.indexOf(' ') + 1);
            this.slider.slideNext();
          }
        });
        console.warn("************ FIREBASE END ************");
      }

    })
    .catch((err) => {
      console.error('home.ts: facebookLogin: Facebook.login catch');
      console.error(JSON.stringify(err));
    })
  }

  /** TODO: Use this function to check that the Facebook user is not already signed-in to Firebase to avoid un-needed re-auth:
  *
  */
  private isUserEqual(facebookUser, firebaseUser): boolean {

    if (firebaseUser) {
      var providerData = firebaseUser.providerData;
      console.warn('home.ts : isUserEqual : providerData');
      console.warn(JSON.stringify(providerData));
      console.warn("providerData.length = " + providerData.length);
      for (var i = 0; i < providerData.length; i++) {
        console.warn("home.ts : inside for loop start");
        if (providerData[i].providerId === firebase.auth.FacebookAuthProvider.PROVIDER_ID &&
            providerData[i].uid === facebookUser.userID) {
          // We don't need to reauth the Firebase connection.
          console.warn("home.ts : isUserEqual : We don't need to reauth the Firebase connection.");
          console.warn("home.ts : isUserEqual : returning true");
          return true;
        }
        console.warn("home.ts : inside for loop end");
      }
      console.warn("home.ts : after for loop");
    }
    console.warn('home.ts : after if firebaseUser');
    console.warn("home.ts : isUserEqual : returning false");
    return false;
  }
  public onCancel(event) {

  }
}
