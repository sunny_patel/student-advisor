import { Component } from '@angular/core';
import { ionicBootstrap, Platform } from 'ionic-angular';
import { StatusBar } from 'ionic-native';

import { HomePage } from './pages/home/home';
import * as firebase from 'firebase';

@Component({
  template: '<ion-nav [root]="rootPage"></ion-nav>'
})
export class MyApp {
  rootPage: any = HomePage;

  constructor(public platform: Platform) {
    let config: any = {
      apiKey: "AIzaSyATkkE0GRs082A6uh1P43EP49Yc1dWCpcw",
      authDomain: "student-advisors.firebaseapp.com",
      databaseURL: "https://student-advisors.firebaseio.com",
      storageBucket: "student-advisors.appspot.com",
    };
    firebase.initializeApp(config);
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }
}

ionicBootstrap(MyApp);
